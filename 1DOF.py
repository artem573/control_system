import math
import numpy as np
import matplotlib.pyplot as plt

J = 5e-3 # [kg m2]
Nt = 2000
phi_0 = 0.
phi_goal = math.pi/2

omega_0 = 0.
dt = 1e-2 # [s]
t = 0. #   [s]
Kp = -0.2
Ki = 0.5
Kd = 0
e_int = 0.
int_window = 30

e = np.zeros(Nt)
t = np.zeros(Nt)
phi = np.zeros(Nt)

for i in range(1,Nt):
    t[i] = t[i-1] + dt
    e[i] = phi_0 - phi_goal
    e_int += e[i]*dt
    if(i>int_window): e_int -= e[i-int_window]*dt
    e_diff = (e[i] - e[i-1])/dt
    M_drive = Kp*e[i] + Ki*e_int + Kd*e_diff
    phi[i] = 1./(2*J)*M_drive*dt**2 + omega_0*dt + phi_0
    
    phi_0 = phi[i]
    omega = 1./(2*J)*M_drive*dt + omega_0 
    omega_0 = omega


plt.plot(t,phi)
plt.show()
